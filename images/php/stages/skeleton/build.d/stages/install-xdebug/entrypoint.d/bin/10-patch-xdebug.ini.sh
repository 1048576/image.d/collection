#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

target_filepath="/etc/php/${BUILD_ARG_PHP_VERSION}/mods-available/xdebug.ini"
source_filepath="/entrypoint.d/templates${target_filepath}"

csh "cp ${source_filepath} ${target_filepath}"

creplace "${target_filepath}" "\\\${XDEBUG_CLIENT_HOST}" "${XDEBUG_CLIENT_HOST}"
